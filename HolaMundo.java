public class HolaMundo {
    //Función principal
    public static void main(String[] args){
        //Comentario de una sola linea de código
        /* 
         * Comentarios de varias lineas de código
        */
        //Mostrar mensaje en consola
        //println realiza un salto de linea
        System.out.println("Hola mundo");
        System.out.print("Mi nombre es Andrés");

        //VARIABLES
        int entero = 10;
        double decimal = 10.5;
        boolean bandera = true;
        //Reservar espacio en memoria
        String mensaje;
        //Asignar datos al espacio en memoria 'mensaje'
        mensaje = "Hola mundo desde una variable de tipo String";
        var dinamica = 'a';
        //Caracter -> a,b,c,d,'1'
        char caracter = 'a';
        //Imprimir mensaje
        System.out.println(mensaje);

        //OPERACIONES
        int n1 = 10;
        int n2 = 20;
        int suma = n1+n2;
        suma += 10;
        int multiplicacion = n1*n2;
        multiplicacion *= 2;
        //Llamar funciones
        double promedio = calcular_promedio(10, 5.5);
        System.out.println("El promedio es: "+promedio);
    }

    //Crear una función calcular promedio
    public static double calcular_promedio(double n1, double n2){
        double promedio = (n1+n2)/2;
        return promedio;
    }

    /* public static void calcular_promedio(double n1, double n2){
        double promedio = (n1+n2)/2;
        System.out.println("El promedio es: "+promedio);
    } */
}
